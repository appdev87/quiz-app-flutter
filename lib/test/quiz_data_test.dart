import 'package:flutter_test/flutter_test.dart';
import 'package:quiz_app/Model/quiz_data_model.dart';
import 'package:quiz_app/ViewModel/quiz_logic_view_model.dart';

void main() {

  test('Test when wrong question is selected', () {
    final quizData = QuizData();
    expect(quizData.question[0].answers[0].correctAnswer, false);

  });

  test('Test when right question is selected', () {
    final quizData = QuizData();
    expect(quizData.question[0].answers[1].correctAnswer, true);
  });
}
