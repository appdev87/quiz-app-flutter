import 'dart:io';

import 'package:flutter/material.dart';
import 'package:quiz_app/View/game_view.dart';
import 'package:quiz_app/ViewModel/custom_text_view_model.dart';

/// {@category Home}
/// {@category Assets, Images, Text, CustomText, SizedBox, MaterialButton, and Column}
/// {@subCategory Home displays}
/// {@image <image alt='' src='/res/images/bckg.png'>}
/// {@font <font src='/res/fonts/Orbitron-VariableFont_wght.ttf' >
class HomeScreenView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
            child: Stack(children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: Image.asset('res/images/bckg.png'),
              ),
              Column(
                textDirection: TextDirection.ltr,
                children: [
                  SizedBox(height: 30),
                  CustomText(
                      title: 'Welcome To',
                      fontSize: 35,
                      fontFamily: 'Parisienne-Regular',
                      color: Colors.deepPurple),
                  SizedBox(height: 10),
                  CustomText(
                      title: 'QUIZ APP',
                      fontSize: 45,
                      fontFamily: 'Orbitron-VariableFont_wght',
                      color: Colors.purple)
                ],
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    MaterialButton(
                      shape: StadiumBorder(),
                      color: Colors.deepPurple,
                      textColor: Colors.white,
                      padding: EdgeInsets.all(20),
                      child: Text('START QUIZ'),
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) {
                          return GameView();
                        }));
                      },
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    MaterialButton(
                      shape: StadiumBorder(),
                      color: Colors.deepPurple,
                      textColor: Colors.white,
                      padding: EdgeInsets.all(20),
                      child: Text('EXIT QUIZ'),
                      onPressed: () {
                        exit(0);
                      },
                    ),
                    SizedBox(
                      height: 30,
                    ),
                  ],
                ),
              ),
            ]),
          ),
        ),
      ),
      backgroundColor: Colors.black87,
    );
  }
}
