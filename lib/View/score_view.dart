import 'dart:io';
import 'dart:developer' as developer;

import 'package:flutter/material.dart';
import 'package:quiz_app/ViewModel/custom_text_view_model.dart';

/// {@category Score}
/// {@category Assets, Images,  CustomText, SizedBox, MaterialButton, and Column}
/// {@subCategory Score displays}
/// {@font <font src='/res/fonts/Orbitron-VariableFont_wght.ttf' >
class ScoreView extends StatelessWidget {
  final int score;

  ScoreView({this.score});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CustomText(
                        title: 'Correct answers',
                        fontSize: 35,
                        fontFamily: 'Orbitron-VariableFont_wght',
                        color: Colors.purple),
                    SizedBox(height: 30.0),
                    CustomText(
                        title: '${this.score}',
                        fontSize: 105,
                        fontFamily: 'Orbitron-VariableFont_wght',
                        color: Colors.purple),
                  ],
                ),
              ),
              SizedBox(height: 150.0),
              MaterialButton(
                  shape: StadiumBorder(),
                  color: Colors.deepPurple,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(20),
                  minWidth: 230.0,
                  child: Text('EXIT QUIZ'),
                  onPressed: () {
                    developer.log('Exit Button pressed in the ScoreView');
                    exit(0);
                  })
            ]),
          )),
      backgroundColor: Colors.black87,
    );
  }
}
