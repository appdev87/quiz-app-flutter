import 'dart:developer' as developer;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:quiz_app/Model/quiz_data_model.dart';
import 'package:quiz_app/View/score_view.dart';
import 'package:quiz_app/ViewModel/quiz_logic_view_model.dart';

/// {@category Score}
/// {@category Assets, Images,  CustomText, SizedBox, MaterialButton, and Column}
/// {@subCategory Score displays}
/// {@font <font src='/res/fonts/Orbitron-VariableFont_wght.ttf' >
class GameView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: GameSetupView(),
    );
  }
}

class GameSetupView extends StatefulWidget {
  @override
  _GameSetupViewState createState() => _GameSetupViewState();
}

/// {@category GameSetup}
/// {@category Assets, Images,  CustomText, SizedBox, MaterialButton, and Column}
/// {@subCategory Quiz displays}
/// {@font <font src='/res/fonts/Orbitron-VariableFont_wght.ttf' >
class _GameSetupViewState extends State<GameSetupView> {
  List<Icon> answers = [];
  int _questionNumber = 0;

  QuizData quizData = QuizData();
  QuizLogic quizLogic = QuizLogic();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        alignment: AlignmentGeometry.lerp(AlignmentDirectional.center, Alignment.topCenter, 15.0),
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 35.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20.0),
                  child: Row(
                    children: answers,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                        child: Text(
                      quizData.question[this._questionNumber].question,
                      style: TextStyle(color: Colors.blueGrey, fontSize: 20.0),
                      softWrap: false,
                      maxLines: 5,
                      // Use to prevent overflow of the text
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.center,
                    )),
                  ],
                ),
                SizedBox(height: 150.0),
                MaterialButton(
                  shape: StadiumBorder(),
                  color: Colors.deepPurple,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(20),
                  child: Text(quizData.question[this._questionNumber].answers[0].possibleAnswer),
                  onPressed: () {
                    setState(() {
                      /// adds the answers in the [answers] list parsing the question found in [QuizData]
                      this.answers.add(quizLogic.checkAnswer(
                          answer: quizData.question[this._questionNumber].answers[0].correctAnswer));

                      this.answers.forEach((element) {
                        developer.log('Current answer records are: $element.');
                      });

                      nextQuestion(getCorrectAnswersScore());
                    });
                  },
                ),
                SizedBox(height: 20.0),
                MaterialButton(
                    shape: StadiumBorder(),
                    color: Colors.deepPurple,
                    textColor: Colors.white,
                    padding: EdgeInsets.all(20),
                    child: Text(quizData.question[this._questionNumber].answers[1].possibleAnswer),
                    onPressed: () {
                      setState(() {
                        /// adds the answers in the [answers] list parsing the question found in [QuizData]
                        this.answers.add(quizLogic.checkAnswer(
                            answer: quizData.question[this._questionNumber].answers[1].correctAnswer));

                        this.answers.forEach((element) {
                          developer.log('Current answer records are: $element');
                        });

                        nextQuestion(getCorrectAnswersScore());
                      });
                    }),
                SizedBox(height: 20.0),
                MaterialButton(
                    shape: StadiumBorder(),
                    color: Colors.deepPurple,
                    textColor: Colors.white,
                    padding: EdgeInsets.all(20),
                    child: Text(quizData.question[this._questionNumber].answers[2].possibleAnswer),
                    onPressed: () {
                      setState(() {
                        /// adds the answers in the [answers] list parsing the question found in [QuizData]
                        this.answers.add(quizLogic.checkAnswer(
                            answer: quizData.question[this._questionNumber].answers[2].correctAnswer));

                        this.answers.forEach((element) {
                          developer.log('Current answer records are: $element');
                        });

                        nextQuestion(getCorrectAnswersScore());
                      });
                    }),
                SizedBox(height: 20.0),
                MaterialButton(
                    shape: StadiumBorder(),
                    color: Colors.deepPurple,
                    textColor: Colors.white,
                    padding: EdgeInsets.all(20),
                    child: Text(quizData.question[this._questionNumber].answers[3].possibleAnswer),
                    onPressed: () {
                      setState(() {
                        /// adds the answers in the [answers] list parsing the question found in [QuizData]
                        this.answers.add(quizLogic.checkAnswer(
                            answer: quizData.question[this._questionNumber].answers[3].correctAnswer));

                        this.answers.forEach((element) {
                          developer.log('Current answer records are: $element');
                        });

                        nextQuestion(getCorrectAnswersScore());
                      });
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// Function to determine if the question pointer should be incremented or if it is the end of [Questions]
  /// Parameter parsed is the number of [correctAnswers] returned by the [getCorrectAnswers] function
  void nextQuestion(int correctAnswers) {
    // Stopping at 9 as arrays start at 0 as usual
    if (this._questionNumber == 9) {
      developer.log('Correct answers before parsing in the constructor is: $this.answers');
      Navigator.push(
          context,
          MaterialPageRoute(
              builder:
                  // Parsed in the correct answers as parameter in the ScoreView constructor
                  (context) => ScoreView(score: correctAnswers)));
    } else {
      this._questionNumber += 1;
    }
  }

  /// Function used to retrieved all the correct answers based on the amount of Green Icon that has been added in the
  /// correctAnswers instance List.
  ///
  /// Returns amount of [correctAnswers] found using the [answers] List
  int getCorrectAnswersScore() {
    int correctAnswers = 0;

    // Retrieve all the correct answers
    this.answers.forEach((icon) {
      if (icon.color == Colors.green) {
        correctAnswers++;
      }
    });

    return correctAnswers;
  }
}
