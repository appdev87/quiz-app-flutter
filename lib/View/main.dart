import 'package:flutter/material.dart';
import 'package:quiz_app/View/home_view.dart';

void main() => runApp(QuizMainView());

/// {@category Main}
/// {@category MaterialApp, SafeArea}
class QuizMainView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(child: HomeScreenView()),
    );
  }
}
