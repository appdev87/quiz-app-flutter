import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// {@category Score}
/// {@category Text}
/// {@subCategory Text custom view}
class CustomText extends StatelessWidget {
  final String title;
  final double fontSize;
  final String fontFamily;
  final Color color;

  const CustomText({this.title, this.fontSize, this.fontFamily, this.color});

  @override
  Widget build(BuildContext context) {
    return Text(
      this.title,
      style: TextStyle(
          fontSize: this.fontSize,
          fontWeight: FontWeight.bold,
          color: this.color,
          letterSpacing: 2.0,
          fontFamily: this.fontFamily
      ),
    );
  }
}