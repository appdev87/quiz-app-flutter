import 'package:flutter/material.dart';

/// {@category QuizLogic}
/// {@category Assets, Images, Icons}
class QuizLogic {

  Icon checkAnswer({bool answer}) {
    if (answer) {
      return Icon(Icons.adb, color: Colors.green, size: 20.0);
    } else {
      return Icon(Icons.bug_report, color: Colors.red, size: 20.0);
    }
  }

}
