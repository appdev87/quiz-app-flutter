import 'answer_model.dart';

/// Class used as placeholder for the [question] and its according [answers]
class Question {
  final String question;
  final List<Answer> answers;

  Question({this.question, this.answers});

}