
/// Class used mainly for structure of `String` [answer] and `bool` [correctAnswer]
class Answer {

  String possibleAnswer;
  bool correctAnswer;

  Answer({this.possibleAnswer, this.correctAnswer});
}