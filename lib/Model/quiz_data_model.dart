import 'package:quiz_app/Model/answer_model.dart';
import 'package:quiz_app/Model/question_model.dart';

/// Main data class used as the structure for the [question]
class QuizData {
  List<Question> question = [
    Question(
        question:
            'How do you top center a button?',
        answers: [
          Answer(possibleAnswer: 'android:gravity="top|middle"', correctAnswer: false),
          Answer(possibleAnswer: 'android:gravity="top|center"', correctAnswer: true),
          Answer(possibleAnswer: 'android:align="top|center"', correctAnswer: false),
          Answer(possibleAnswer: 'android:alignment="top|center"', correctAnswer: false)
        ]),
    Question(
        question:
            'What type of layout generates multiple orientation?',
        answers: [
          Answer(possibleAnswer: 'Inline layout', correctAnswer: false),
          Answer(possibleAnswer: 'Nested layout', correctAnswer: true),
          Answer(possibleAnswer: 'Nested View orientation', correctAnswer: false),
          Answer(possibleAnswer: 'Inner layout', correctAnswer: false)
        ]),
    Question(
        question:
            'How would you call the following button declared in your xml as "android:id=\"@+id\\myButton\"?',
        answers: [
          Answer(possibleAnswer: 'Button myButton = findViewById(R.id.myButton);', correctAnswer: true),
          Answer(possibleAnswer: 'Button myButton = R.get.Id("myButton");', correctAnswer: false),
          Answer(possibleAnswer: 'Button myButton = findViewById("myButton");', correctAnswer: false),
          Answer(possibleAnswer: 'Button myButton = new Button(R.id.myButton);', correctAnswer: false)
        ]),
    Question(
        question:
        'How to make a View match the size of its parent?',
        answers: [
          Answer(possibleAnswer: '"wrap-content"', correctAnswer: false),
          Answer(possibleAnswer: '"match-parent"', correctAnswer: true),
          Answer(possibleAnswer: 'min-width="wrap_view"', correctAnswer: false),
          Answer(possibleAnswer: 'android:layout_width="match-parent"', correctAnswer: false)
        ]),
    Question(
        question:
        'What are the ways to align text?',
        answers: [
          Answer(possibleAnswer: 'textAlignment & textOrientation', correctAnswer: false),
          Answer(possibleAnswer: 'textDirection & textAlignment', correctAnswer: false),
          Answer(possibleAnswer: 'textAlignment & textStyle', correctAnswer: false),
          Answer(possibleAnswer: 'textAlignment & gravity', correctAnswer: true)
        ]),
    Question(
        question:
        'On App re-launch, what method can be used to keep on where you left off?',
        answers: [
          Answer(possibleAnswer: 'onNewIntent(Intent newIntent)', correctAnswer: true),
          Answer(possibleAnswer: 'onCreate()', correctAnswer: false),
          Answer(possibleAnswer: 'onAppLaunch()', correctAnswer: false),
          Answer(possibleAnswer: 'appEnteredBackground()', correctAnswer: false)
        ]),
    Question(
        question:
        'What method is used for logging errors?',
        answers: [
          Answer(possibleAnswer: 'Log.e()', correctAnswer: true),
          Answer(possibleAnswer: 'System.out.println()', correctAnswer: false),
          Answer(possibleAnswer: 'print()', correctAnswer: false),
          Answer(possibleAnswer: 'System.out.err()', correctAnswer: false)
        ]),
    Question(
        question:
        'Which of these are ACTIVITY states?',
        answers: [
          Answer(possibleAnswer: 'start, play, pause, stop', correctAnswer: false),
          Answer(possibleAnswer: 'begin, run, pause, stop, destroy', correctAnswer: false),
          Answer(possibleAnswer: 'start, run, pause, stop, destroy', correctAnswer: true),
          Answer(possibleAnswer: 'play, pause, stop', correctAnswer: false)
        ]),
    Question(
        question:
        'What approach does the ROM in SQLite use?',
        answers: [
          Answer(possibleAnswer: 'SQLite framework approach', correctAnswer: false),
          Answer(possibleAnswer: 'The Maven approach', correctAnswer: false),
          Answer(possibleAnswer: 'The Oracle approach', correctAnswer: false),
          Answer(possibleAnswer: 'The hibernate approach', correctAnswer: true)
        ]),
    Question(
        question:
        'Android can be built in Flutter ...',
        answers: [
          Answer(possibleAnswer: 'with Native functionalities', correctAnswer: false),
          Answer(possibleAnswer: 'using the Dart programming language as the main framework', correctAnswer: true),
          Answer(possibleAnswer: 'only when a physical device is connected', correctAnswer: false),
          Answer(possibleAnswer: 'using only SQLite as its database', correctAnswer: false)
        ]),
  ];
}
